+++
title = "Publications"
date = 2023-12-20
sort_by = "weight"
weight = 2
+++

In the following section, you may find details regarding my publications
in academia, invited talks, etc. You may similarly find the academic parts
of this list on [my ORCID page](https://orcid.org/0009-0003-3271-3558).
