+++
title = "Differential Fuzzing of the Solana BPF VM’s Just-in-Time Compiler and Interpreter"
date = 2022-05-11
+++

A two-part blog series posted on [secret.club](https://secret.club) under the
title: "Earn $200K by fuzzing for a weekend". Clickbait-y, yes, but I thought
it was important to get people looking into differential fuzzing :)

[Part 1](https://secret.club/2022/05/11/fuzzing-solana.html) details the
process of developing the increasingly specialised fuzzers which targeted the
Solana Berkeley Packet Filter Virtual Machine.

[Part 2](https://secret.club/2022/05/11/fuzzing-solana-2.html) details the
debugging process for each discovered bug along with the details originally
provided to Solana.

Proceeds from these bug bounties were used to create [an endowment for the
Texas A&M Cybersecurity Club and a software testing course at Texas A&M
University](https://www.txamfoundation.com/Fall-2022/Cyber-Education.aspx).
