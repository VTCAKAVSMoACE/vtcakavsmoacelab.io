+++
title = "Miscellaneous"
date = 2023-12-20
weight = 5
+++

This section contains miscellaneous publications, such as blog posts
on other sites or projects that sit on some git or another somewhere.

2022
-----

[Differential Fuzzing of the Solana BPF VM’s Just-in-Time Compiler and Interpreter](solana)

2021
-----

[Schadenfreude: Resurrection](schadenfreuderesurrection)