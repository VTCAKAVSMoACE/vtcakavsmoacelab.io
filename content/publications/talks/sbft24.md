+++
title = "SBFT'24/AIST'24: SoKotHban: A King-of-the-Hill-style Search Optimisation Competition"
date = 2024-04-14
+++

This will be a presentation offered at the Search-Based and Fuzz Testing Workshop and at the Artificial Intelligence in Software Testing in 2024.

[SBFT'24 slides provided here](https://docs.google.com/presentation/d/19wGqL-CZkbapVIU96LNHbhFBGH9saqszYpfRLvrbHp0/edit?usp=sharing).

[Tutorials information offered by SBFT'24](https://sbft24.github.io/tutorials/).

[AIST'24 slides provided here](https://docs.google.com/presentation/d/1rEGi476dFwIVlRbWj4mUTzJc6Ab9G1FSF5fjPUR3a-Y/edit).

[Tutorials information offered by AIST'24](https://aistworkshop.github.io/).

### Abstract

Sokoban, a puzzle game developed in the early 1980s, has proven to be a challenging search optimisation problem often aligned with autonomous agent research. The eponymous "warehouse keeper" attempts to move crates into desired positions, often encountering difficult terrain and situations as crates the players themselves have moved block the path. These puzzles prove to be difficult for humans and computers alike, and general-purpose solving for Sokoban puzzles is PSPACE-complete as shown by previous research. To improve the academic understanding of the problem, we propose a King-of-the-Hill (KotH) competition in which competitors develop both automated puzzle solvers and generators. With this, we hope to identify what puzzle features weaken which optimisation strategies, what strategies can be used to defeat new challenges, and observe specialised adversarial techniques to target and disrupt tactics employed by various competitors.
