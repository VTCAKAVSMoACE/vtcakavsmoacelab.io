+++
title = "SBFT'23: AFLrustrust and LibAFL_libFuzzer"
date = 2023-05-14
+++

This was a presentation offered during SBFT'23 to describe the
[LibAFL](https://github.com/AFLplusplus/LibAFL) team's submissions
to the fuzzing tool competition.

[Slides presented](/pdfs/AFLrustrust%20and%20LibAFL_libFuzzer.pdf).

[Recording available on YouTube (starting at 4:45:08)](https://youtu.be/EF13eiidhA0?feature=shared&t=17108).
