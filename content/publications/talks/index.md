+++
title = "Talks"
date = 2023-12-20
updated_date = 2024-09-27
weight = 2
+++

This section contains recordings, slides, or other information
regarding talks and presentations I've given. This is not an
exhaustive list!

2024
-----

[Vector Automotive Cybersecurity Symposium: To Fuzz, or Not To Fuzz](vector-acs)

[SBFT'24: SoKotHban: A King-of-the-Hill-style Search Optimisation Competition](sbft24)

[Trail of Bits "Lunch and Learn": Underutilised Fuzzing Strategies for Modern Software Testing](tobunderutilised)

2023
-----

[37c3: Fuzz Everything, Everywhere, All at Once](37c3)

[SBFT'23: AFLrustrust and LibAFL_libFuzzer](sbft23)
