+++
title = "Vector Automotive Cybersecurity Symposium: To Fuzz, or Not To Fuzz"
date = 2024-07-01
updated_date = 2024-09-27
+++

Video is available on [YouTube](https://youtu.be/0OajN0Ifesk?si=U7iuCXsgELalhNh0).

Presentation slides are available on [Google Slides](https://docs.google.com/presentation/d/1p4cYSYVjWPfQIoyK43zLtLkAv3YAQsa2W0hbwzpLE3s/edit?usp=sharing).

### Abstract

Fuzzing is beautiful: powerful, automated, simple, elegant, and effective.
Touted as one of, if not the, most powerful testing strategy available, fuzzing has seen widespread use and been the subject of great research in both industry and academia.
Yet, what is fuzzing, and how is it actually used? What are the bounds of this powerful strategy, and what pitfalls do we see in practice?
This talk aims to contextualise fuzzing in the broader testing domain, showing that it is one piece of the much larger puzzle.
