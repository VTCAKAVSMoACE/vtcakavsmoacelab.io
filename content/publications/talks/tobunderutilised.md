+++
title = "Trail of Bits \"Lunch and Learn\": Underutilised Fuzzing Strategies for Modern Software Testing"
date = 2024-01-17
+++

Talk is available online on [YouTube](https://www.youtube.com/watch?v=fMzeIv4U4LI&pp=ygUNdHJhaWwgb2YgYml0cw%3D%3D).

Slides are available on [Google Slides](https://docs.google.com/presentation/d/1nWPZLKMlKUcjsC-YQD703ZpoaAcBDXHw3Vo9DNRXL3o/edit?usp=sharing).

### Abstract

Fuzzing is a well-researched and understood concept, but
so little of its potential is leveraged. Throughout the talk, I
introduce key features of LibAFL being used to research the
frontier of fuzzing. We will use this to explore the power and
problems of alternative fuzzing oracles. It is my hope that
listeners walk away with a shift in perspective regarding the
use of fuzzing and with excitement for its future potential.
