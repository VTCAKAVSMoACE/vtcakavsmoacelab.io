+++
title = "Papers"
date = 2023-12-20
sort_by = "date"
weight = 1
+++

You may find each of the papers in this section on [my ORCID page](https://orcid.org/0009-0003-3271-3558).

2024
-----

[SoK: Prudent Evaluation Practices for Fuzzing](fuzzeval)

2023
-----

[CrabSandwich: Fuzzing Rust with Rust (Registered Report)](crabsandwich)

[AFLrustrust: A LibAFL-based AFL++ prototype](aflrustrust)

[LibAFL_libFuzzer: libFuzzer on Top of LibAFL](libafllibfuzzer)

2022
-----

[TheHuzz: Instruction Fuzzing of Processors Using Golden-Reference Models for Finding Software-Exploitable Vulnerabilities](thehuzz)

2021
-----

[OpenRace: An Open Source Framework for Statically Detecting Data Races](openrace)
