+++
title = "CrabSandwich: Fuzzing Rust with Rust (Registered Report)"
date = 2023-07-17
+++

[Available with paywall online from ACM](https://dl.acm.org/doi/10.1145/3605157.3605176).

[Available online for free from EURECOM](https://www.s3.eurecom.fr/docs/fuzzing23_crump_report.pdf).

[Source code available on the LibAFL GitHub](https://github.com/AFLplusplus/LibAFL/tree/main/libafl_libfuzzer).
