+++
title = "SoK: Prudent Evaluation Practices for Fuzzing"
date = 2024-05-20
+++

Publication is provided on [the primary author's website](https://mschloegel.me/paper/schloegel2024sokfuzzevals.pdf).

[Source code and other artifacts available online](https://github.com/fuzz-evaluator/).
