+++
title = "LibAFL_libFuzzer: libFuzzer on Top of LibAFL"
date = 2023-05-01
+++

[Available with paywall online from IEEE](https://ieeexplore.ieee.org/document/10190370).

[Available online for free from AFL++ website](https://aflplus.plus/papers/libafl_libfuzzer_sbst23.pdf).

[Source code available on the LibAFL GitHub](https://github.com/AFLplusplus/LibAFL/tree/main/libafl_libfuzzer).
