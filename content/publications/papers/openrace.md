+++
title = "OpenRace: An Open Source Framework for Statically Detecting Data Races"
date = 2021-11-01
+++

[Available with paywall online from IEEE](https://ieeexplore.ieee.org/document/9651289).

[Available online for free from Yanze Li's website](https://liyz.pl/index/correctness21-openrace.pdf).

