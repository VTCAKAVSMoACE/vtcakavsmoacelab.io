+++
title = "AFLrustrust: A LibAFL-based AFL++ prototype"
date = 2023-05-01
+++

[Available with paywall online from IEEE](https://ieeexplore.ieee.org/abstract/document/10190379).

[Available online for free from AFL++ website](https://aflplus.plus/papers/aflrustrust-sbst23.pdf).

[Source code available on the LibAFL GitHub](https://github.com/AFLplusplus/LibAFL/tree/main/fuzzers/fuzzbench_forkserver).
