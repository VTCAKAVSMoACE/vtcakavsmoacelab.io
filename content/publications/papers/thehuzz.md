+++
title = "TheHuzz: Instruction Fuzzing of Processors Using Golden-Reference Models for Finding Software-Exploitable Vulnerabilities"
date = 2022-08-01
+++

[Available online for free from USENIX](https://www.usenix.org/conference/usenixsecurity22/presentation/kande).

