+++
title = "Introduction"
date = 2023-12-20
sort_by = "weight"
+++



### Welcome to my home!

If you're here for my contact info, the best way to reach me is by [email](mailto:me@addisoncrump.info). If you
want to reach out to me another way, I'm most reachable on <a href="https://github.com/addisoncrump" rel="me">GitHub</a> and <a href="https://nothing-ever.works/@addison" rel="me">Mastodon</a>.

### Who are you, anyways?

My name is Addison Crump. I'm a Texas A&M University graduate (class of '21) and currently
a Ph.D. candidate at <a href="https://cispa.de/en/people/c01adcr" rel="me">CISPA</a>. I'm also a member of
<a href="https://secret.club/author/addison" rel="me">secret.club</a> and a maintainer of
[LibAFL](https://github.com/AFLplusplus/LibAFL).

I've done vulnerability research, programming, and systems administration for years. Feel
free to reach out if you're recreating some of my work or just want to chat (email above!).
