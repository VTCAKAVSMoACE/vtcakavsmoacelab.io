+++
title = "Lessons Learned from Automotive Cybersecurity"
date = 2024-09-27
+++

I was recently [invited to speak at an automotive cybersecurity conference](../../publications/talks/vector-acs) by [Vector](https://www.vector.com/int/en/), a development group for cyberphysical systems[^1].
Specifically, I was invited by [Dr.rer.nat. Till Neudecker](https://www.dsn.kastel.kit.edu/english/staff_neudecker.php) to give a talk about fuzzing as it has seemingly not seen too much uptake in the automotive industry.
This will soon change with the publication of the new [Automotive SPICE for Cybersecurity](https://en.wikipedia.org/wiki/Automotive_SPICE) model, which I believe will explictly list fuzzing as a maturity measurement.

This conference was very eye-opening as to the current state of automotive security practices and, looking forward, what seems to be the next steps for the industry.

**_All items expressed here are my opinion, based on limited exposure to the current state of testing. I make no guarantees about the correctness of any of this, and instead provide what I experienced/understood from conversations with other professionals who attended the event._**

### Regarding fuzzing topics

There were a number of talks which discussed fuzzing -- either in passing or as the overall theme -- as well as booths promoting dedicated fuzzing tools.
Like many industries, there is sadly a disconnect between the academic state of the art for fuzzing and industry practices, though the fuzzing tools I personally observed were designed in a way that was composable with existing strategies.
These tools were presented publicly and the conference itself was considered "public", but I will refrain from discussing these tools at length with respect for the privacy of the companies as they continue to develop these tools.

From my perspective, there has been significant engineering effort dedicated to the harnessing of _arbitrary physical and emulated devices_, allowing for fuzzing of network stacks.
This poses a significant development in the testing of networked systems.
On the other hand, there has not been much progress in acquiring guidance or oracle feedback yet -- which, again, is quite understandable as these tools have not yet reached great maturity, and the tooling is designed in a manner that may allow for greater support for these strategies in the future.
Similarly, some discussion regarding these tools indicated lack of awareness of some modern fuzzing techniques that can quickly address these issues.
To mind, I suggested: [Nyx-Net](https://dl.acm.org/doi/pdf/10.1145/3492321.3519591), [Hoedur](https://www.usenix.org/conference/usenixsecurity23/presentation/scharnowski), [my unicode mutations from LibAFL](https://docs.rs/libafl/latest/libafl/mutators/unicode/index.html) (for the fuzzing of string-like types), [Silifuzz](https://arxiv.org/abs/2110.11519), and otherwise a variety of firmware fuzzing papers.

This disconnect represents what I believe to be a communication failure on the part of academia and the fuzzing community at large as to providing effective means for teams to get started with fuzzing, and to know what has already been explored.
We would do well to better systematise existing papers by topic and impact so that professionals in industry and open source sectors may more effectively use our results.

### Regarding system design

Quite a few conversations at the conference centered around the concept of "software-defined vehicles" (SDVs), a strategy which proposes to centralise the logic of vehicles to a single ECU with shared hardware resources to reduce OTA update complexity and cost.
Engineers proposing this topic primarily communicated this as placing the logic components together on a single board and manage separation by hypervisors.

Security engineers present generally were not in favour of this design, citing concerns regarding the security of the hypervisor and single-point-of-failure problems associated with the centralised hardware.
Proponents highlighted the simplicity of update rollout but admitted some components could not be centralised (such as those which require some location).

**_In my opinion_**, this is a design error that will simplify design initially at the long-term cost of unexpected failures, software complexity increases, and safety issues.
Additionally, this defers responsibility of secure design to external companies responsible for software development, especially of the security-relevant components (most critically, the hypervisor).
I think these decisions are convenience motivated that will result in long-term problems.

### Regarding network security

As it turns out, cars nowadays often look like wired networks -- often literally using ethernet as connection medium -- and suffer the same issues of security as these other systems.
Significant effort is being placed into converting these networks to use [MACsec](https://en.wikipedia.org/wiki/IEEE_802.1AE), [CANsec](https://www.renesas.com/en/blogs/art-networking-series-8-cansec-can-xl-layer-2-security-protocol), and [IPsec](https://www.autosar.org/fileadmin/standards/R21-11/AP/AUTOSAR_EXP_IPsecImplementationGuidelines.pdf) (note the name collision with VPN protocols).
This seems to be a good step in security these networks against security issues associated with impersonation, but do not mitigate issues with undesired communication by compromised devices.

To mitigate issues with compromised or untrusted devices, both vehicle-deployed firewalls and IDSes have been suggested.
The firewalls are software-defined at network switch level, and enforce communication routes between devices.
Several proposed domain-specific languages for enforcing this have been proposed.

IDSes, on the other hand, attempt to find evidence of device compromise and undesired communication between devices.
So far, this technology seems to be in its infancy for vehicles -- manufacturers face cost issues associated with implementing such systems, given high data throughput and cost limitations.
In typical IDSes, they either define pre-defined rules about behaviour -- which are necessarily limited and incomplete -- or use machine learning to detect unwanted behaviour, such as anomaly detection or classification.
The IDSes deployed in "classical" networks with the level of throughput and responsivity requirements observed on vehicles typically use fairly beefy FPGAs to accelerate the detection and allow for updates, but, in this case, this is totally infeasible from a cost perspective.
The IDSes on vehicles also cannot realistically block traffic due to safety concerns, and are thus limited to reporting only.
In practice, this means that these systems can only realistically be used post-mortem and must be inspected by an upstream engineer, which, at the scale of deployment we see in the automotive industry, is likely prohibitively expensive.

**_In my opinion_**, the requirement for vehicles to have onboard IDSes will simply be used as a checkbox that will be cost-deferred to consumers with very little benefit.
The effort being placed into securing these networks by segmentation (firewalls, transport-level authentication, etc.) will be more valuable in the long run.

### Regarding the uptake of memory-safe languages

I am very pleased to share that there seem to be great advances in the adoption of Rust and other memory safe languages.
Though adoption is slow, multiple developers indicated to me that their company had made commitments to develop new firmware strictly in Rust, abandoning older and obselete systems.
Conversely, compliance issues are still blocking adoption in particular areas, especially in that of cryptography, due to certification of certain implementations.
Especially of note was that the Rust compiler is not sufficiently controllable to guarantee e.g. timing-safety, use of secure memory, and other items without calling out to C/C++ implementations.

That said, it is clear that safety-critical components are beginning to move in the direction of memory-safe languages, especially given an observed reduction of memory-safety issues that would have otherwise had cost implications for the vendors.

### Personal concerns and observations

The conference was obviously biased towards security and compliance teams.
Several of these engineers indicated some distress regarding the rate at which automotive products are increasing in complexity without a corresponding increase in testing.
Similarly, I got the feeling that people deeply questioned the purpose of some of the new systems being put into production.
In effect: most of these new systems are _convenience_ features which provide few safety benefits, but distinguish the vehicles for customers in terms of _luxury_ or _novelty_.
The push for new features necessarily leads to greater interconnectivity and complexity at the cost of security and safety, which will only realistically affect customers negatively.
Similarly, the technology is advancing far faster than compliance, and existing regulations are too easily shrugged off as bureaucratic processes rather than encouraging deeper testing and design safety.

## Conclusions

The conference was very eye-opening to attend.
There are great steps forward being made in automotive security, though design decisions are being made that may jeopardise certain safety guarantees.
Academia and corresponding testing communities must better communicate testing strategies which simplify and improve testing that would be useful for automotive applications, especially in terms of fuzzing.

Overall, while there are some concerns yet, we are seeing improvements to how vehicles are being tested and designed.

[^1]: AFAICT they are involved in at least medical and automotive applications.
