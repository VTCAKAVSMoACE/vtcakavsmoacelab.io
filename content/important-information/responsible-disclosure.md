+++
title = "Responsible Disclosure"
date = 2023-12-20
sort_by = "weight"
weight = 3
+++

Developers of software or designs under test in a security research context
must be informed of all vulnerabilities discovered during that research **within
90 days of discovery, with no exceptions**.

Research strategies and materials capable of discovering previously unknown
or undiscoverable vulnerabilities or of discovering known vulnerabilities better
than the existing state of the art must be published **within 360 days of
initial development, with no exceptions**.

### Ethical Considerations

As researchers, our first and foremost responsibility is to protect others.

If, in any way, something found in the course of research could be used to harm
another individual, it is our duty to remediate it to the best of our ability.
No exception may be made for personal benefit or allegiance. The mere existence
of a vulnerability or security research strategy which could be used to discover
vulnerabilities "in the wild" is no different to the existence of a backdoor;
if one individual or group may use it, any may for any reason, by rediscovery,
espionage, or improper use by a "good" actor.

Thus, it is critical that, regardless of the potential (ab)use of a
vulnerability, it is the researcher's responsibility to ensure it is remediated.
Similarly, research strategies which prove to discover vulnerabilities must be
made available in an accessible, usable format such that developers and end users
have the means to protect themselves and prevent future vulnerabilities.

## Vulnerabilities

### Disclosure Information

All disclosures should contain:
- details on the software or design affected
- observations regarding the vulnerability
- reproduction information (if available)
- workaround information (if available)
- remediation information (if available)
- information regarding the discovery of the vulnerability
- discussion on the likelihood of rediscovery by others
- a timeline, including:
  - a vendor disclosure deadline of **no greater than 45 days** from first disclosure to developer (if relevant)
  - a governmental disclosure deadline of **no greater than 90 days** from first disclosure to developer
  - a public disclosure deadline of **no greater than 180 days** from first disclosure to developer
- a genuine offer to assist in the remediation and public disclosure process

Developers are entitled to information regarding why and how their software
or design was targeted and researched. Every reasonable step must be taken
by the researcher to ensure that the developer can utilise the provided
information, taking into account possible accessibility requirements due
to language differences, disability, etc.

### Regarding Payment Associated with the Disclosure of Vulnerabilities

Payment should not be expected or sought out, only graciously accepted when offered without prompt.

There's a great deal of things wrong with bug bounties offered by
companies (e.g. low payouts considering potential impact, intentional
disregard of vulnerability impact by bounty providers, etc.). This
does not justify "revenge" public disclosure or failing to disclose;
it is the researcher's responsibility to disclose regardless.

Sales of vulnerabilities to private third-parties or governments
is unjustifiable as it actively removes the developer's ability
to remediate.

### Regarding Unwilling Developers

If a developer is unwilling, unreachable, or otherwise will not/cannot 
remediate a vulnerability, the researcher is responsible for ensuring
that the public is made aware of and able to protect themselves from any
threat caused by the vulnerability.

In the original disclosure to the developer, a timeline of separate
disclosure is provided. This is always included, regardless of whether
this e.g. invalidates bug bounty offerings or leads to legal threat<sup id="footnote-1-backref"><a href="#footnote-1">1</a></sup>.
For each deadline that passes, the
1. vendor or distributor of the software or design is notified (e.g. Ubuntu foundation, OEMs)
2. appropriate government is notified (e.g. US-CERT, BSI, etc.)
3. the public is notified by means of open, widely-available online publication.

## Research Strategies and Materials

Just like vulnerability data, research strategies with measurable ability
to discover new vulnerabilities or improve upon the state of the art in
discovering known vulnerabilities must be published. For the same reasons
that vulnerabilities should be disclosed for remediation, strategies or
materials capable of discovering vulnerabilities should be made available
to the public such that developers or other researchers may use them for
remediating new or existing vulnerabilities.

Just like vulnerabilities, retention or sale of research strategies for
personal gain or allegiance is unjustifiable. We cannot realistically
use our research to its fullest extent, and thus in-effect fail to
disclose vulnerabilities discoverable by our research in either case.

### Format and Availability

Research ideas, strategies, results, and artifacts must be made publicly
available **within 360 days of initial development, with no exceptions**.
It must be published in a format which is accessible and usable by others
with a reasonable level of proficiency in the domain which the research
targets.

As an example: If one were to develop a fuzzer for JavaScript programs,
it must be reasonable to expect that a proficient JavaScript developer
is able to a) access, b) understand the concepts of, and c) use the
fuzzer.

### Use Before Publication

When developing a new general strategy, it is unreasonable to expect that
the researcher test _every_ possible affected software or design. If a
strategy demonstrates the ability to discover new vulnerabilities in a
sample of targets, a collection of the most widely-used pieces of software
or designs realistically affected by the strategy should be thoroughly
tested to ensure that the publication of the research would not lead to
the in-effect public disclosure of any widespread vulnerabilities.

This does not adjust the 360-day deadline for public disclosure, and
should not be used as an excuse to delay disclosure.

## Closing

Ultimately, in security research, we handle vulnerabilities that impact
the safety of others. Responsible disclosure is a necessary part of
ensuring that our work is used strictly for good.

-----

<sup id="footnote-1"><a href="#footnote-1-backref">1</a></sup>Obviously, protect yourself. If you don't have the means to fight this, don't do it on your own; seek legal assistance from your institution or an appropriate agency.
