+++
title = "Important Information"
date = 2023-12-20
sort_by = "weight"
weight = 1
+++

The following section contains the more serious notes on this website.
Don't worry, I'm not a zealot! I provide this information in the
interest of making things clear to:
 - employers, supervisors, or anyone who may offer funding to me
 - fellow researchers, colleagues, or anyone I may potentially collaborate with
 - students, interns, or anyone I may potentially advise or supervise

Do not read any part of this section as judgement on others. Merely,
this section contains information about personal boundaries and
limitations regarding research and education. Read any section here
as, "if you're working with me, these are my ground rules".

New sections may appear as they become relevant. If you think I've
overlooked something important, please [reach out and let me know](/).
