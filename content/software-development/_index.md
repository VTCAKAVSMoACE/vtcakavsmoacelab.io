+++
title = "Software Development"
date = 2023-06-17
weight = 100
+++

I think about software development sometimes.
It's like it's relevant to my job or something!

Here you can find some collected notes that I found useful and/or documentation for my future self.
