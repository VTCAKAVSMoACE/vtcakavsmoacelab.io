<a rel="license" href="http://creativecommons.org/licenses/by/3.0/us/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/us/88x31.png" /></a><br />This
work is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/us/">Creative
Commons Attribution 3.0 United States License</a>.

That being said, I doubt anyone will steal my work anyways. If you want to use
something from here, please do attribute me! It took a lot of time to write some
of this.
