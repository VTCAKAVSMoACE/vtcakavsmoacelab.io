+++
title = "Academia"
date = 2018-12-30
+++

I'm currently a student, so naturally academics are rather important to me.

## What's in here?

This section serves as a location for me to write about school projects,
organisations, and other academic activities.

## What should I expect to see here?

Anything academic, officially school-sponsored, or loosely related to academic
activities. Hell, I might even talk about high school reunions or something.
We'll just have to see.
