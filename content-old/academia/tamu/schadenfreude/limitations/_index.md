+++
title = "Limitations"
weight = 4
sort_by = "weight"
date = 2021-01-22
+++

This section contains the limitations of
[Schadenfreude: Resurrection](https://gitlab.com/addison-and-teddy/schadenfreude/-/tree/resurrection) based on both
limitations of the theory and limitations of the implementation.
