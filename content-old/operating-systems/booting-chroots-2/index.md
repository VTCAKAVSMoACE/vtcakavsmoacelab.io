+++
title = "Booting chroots: Preparing the chroot"
weight = 2
date = 2019-02-11
+++

We need to modify our chroot a little bit; right now, it's just a husk of an OS,
and isn't bootable.

Any path listed in this section is relative to the root of the chroot.

First of all, we gotta install some things on the chroot, the first of which is
the linux kernel itself:

```bash
[root@chroot ~] # pacman -Sy linux
```

Next, we need to modify our
[mkinitcpio](https://wiki.archlinux.org/index.php/mkinitcpio) configuration to
build an appropriate ramdisk. Depending on your configuration, this will differ
wildly from my own, but below is what I ended up with after many trials:

```
# /etc/mkinitcpio.conf

MODULES=()
BINARIES=()
FILES=()

# Pay very close attention to this; these determine what modules and scripts are loaded into ramdisk and the order
# in which they are run. If you use LUKS over LVM, this will work for you. Otherwise, you *need* to change this to fit
# your configurations.
HOOKS=(base udev autodetect systemd keyboard sd-vconsole modconf block sd-encrypt sd-lvm2 filesystems fsck)
```

We also need to change the presets files to represent our created image. You
should modify this for your target OS, but I suggest prefixing the name of the
OS with 0 so that the host OS doesn't default to it when we make our GRUB entry
later.

```
# /etc/mkinitcpio.conf.d/linux.preset

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-0blackarch"

PRESETS=('default' 'fallback')

default_image="/boot/initramfs-0blackarch.img"

fallback_image="/boot/initramfs-0blackarch-fallback.img"
fallback_options="-S autodetect"
```

```
# /etc/mkinitcpio.conf.d/linux-lts.preset

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-0blackarch-lts"

PRESETS=('default' 'fallback')

default_image="/boot/initramfs-0blackarch-lts.img"

fallback_image="/boot/initramfs-0blackarch-lts-fallback.img"
fallback_options="-S autodetect"
```

If you need to use sd-vconsole, you will need to create an
[`/etc/vconsole.conf`](https://www.freedesktop.org/software/systemd/man/vconsole.conf.html)
with a KEYMAP entry.

If you need to use sd-encrypt, you should copy `/etc/crypttab` from the host
OS's root to `/etc/crypttab` in the chroot.

You absolutely should copy `/etc/fstab` from the host OS's root to `/etc/fstab`
in the chroot. I suggest replacing all the relevant paths in there with UUIDs
using the output of `blkid`.

## Generating initramfs

If you left the chroot to perform any of the previous steps, go ahead and
re-enter it. Then execute:

```bash
[root@chroot ~] # mkinitcpio -p linux -k <kernel>
```

Where you replace <kernel> with the latest kernel version listed in
`/lib/modules`. At the time of writing, my version was `4.20.7-arch1-1-ARCH`.
Note that you must do this or the `mkinitcpio` command will attempt to use the
kernel version of the host OS.

## Next steps

Now we need to
[write the GRUB configuration and the init script](../booting-chroots-3). Not
too much left.
