+++
title = "Languages"
+++

Just to be clear: this is lumping together both spoken and programming
languages.

## What's in here?

Here I will put all of my deep dives into uses of languages. This is not
relating to specific projects. This segment is explicitly for when I dabble
in language features or libraries and I choose to share some of my adventures,
whether successful or otherwise.

## What should I expect to see here?

I'm fluent in Java, C++, and bash, but I stumble around with languages like
Rust, Ruby, Python, Assembly, Malbolge, and so on. Expect a good mix of just
about everything in here. Oh, and I'm trying to learn Albanian. That might make
it here at some point, too.
