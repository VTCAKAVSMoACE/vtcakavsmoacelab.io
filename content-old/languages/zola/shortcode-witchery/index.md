+++
title = "Shortcode Wizardry"
date = 2020-06-21
+++

Or: How I Came to Hate Template Languages Even More

### Templates in Zola

Templates in Zola are based off of Tera templates, with a couple of key differences.

For example, while writing [a post on template injection](../../../security/ctfs/smc3/wh02), I injected myself because
the [comment syntax is different](https://www.getzola.org/documentation/content/shortcodes/#shortcodes-without-body).

Stuff like this is pretty common. But I also had to rewrite the menu for my website because its definitions for what is
and what isn't a subsection are shaky at best.

I invite you to review that
[git commit](https://gitlab.com/VTCAKAVSMoACE/vtcakavsmoace.gitlab.io/-/commit/860a785bca6ed4f8494f72d9b7b307f637f6e3f3)
yourselves.
