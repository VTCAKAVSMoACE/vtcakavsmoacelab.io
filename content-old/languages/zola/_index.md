+++
title = "Zola"
+++

Zola, the static site generator I use, tends to end up with some tomfoolery occurring.

In this section, you get to see my extensive pain and suffering while using this delightful tool. :)

Oh, and if you want to suffer too: go and [get zola](https://getzola.org)!
