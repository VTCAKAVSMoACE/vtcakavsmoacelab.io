+++
title = "Security"
+++

It's pretty obvious that I'm interested in security. Commentaries on
cybersecurity and privacy will be found here.

## What's in here?

You'll find scribblings about security involving breaches, exploits,
vulnerabilities, and tools, as well as comments on privacy, whether that be
personal privacy, corporate misdeeds, or really anything that I just feel
strongly about.

## What should I expect to see here?

Technical commentaries and rants. Really expect anything here.
