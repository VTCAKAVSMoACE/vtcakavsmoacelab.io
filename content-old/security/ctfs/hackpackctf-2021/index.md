+++
title = "HackPack CTF 2021"
date = 2021-04-21
+++

HackPack CTF 2021 took place on the 16th through the 17th of April, 2021, and, while I admit I was late to the 
competition, I had a lot of fun with the challenges I was able to complete!

The first challenge I completed, [better-pam](better-pam), was a faulty authentication system which contained a timing attack 
introduced by `strlen`.

The second challenge I completed, [mind-blown](mind-blown), was a brainfuck interpreter which failed to perform a bounds
check on a stack-contained memory tape, leading to remote code execution with a defeated stack cookie.
