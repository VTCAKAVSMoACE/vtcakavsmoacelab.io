+++
title = "bh01"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/bh01.zip and find a way to extract the flag.
>
>Note that the flag must be submitted as 'normal' text which you might read in a book.

Slapping it open in Ghidra, we find that it calculates to something from the offset of the next instruction using
`__x86.get_pc_thunk.ax` and an `add` instruction to get a position of `0x00010541+0x1a97`. Following it, we find that
it's pointing to the string `...`.

Just above that however, we find `Flag: ` followed by a bunch of non-readable characters. Since it's still
null-terminated, we can make a guess that it's just in a different encoding.

Right-clicking at the start of `Flag: `, we inform Ghidra to treat it as a C string with Data > TerminatedCString. To
change it's encoding, we use Data > Settings... > Charset > UTF-8. Lo and behold:
```text
ꓕꓵoƎꓷISuᴉ
```

Which, if you flip upside down as all beginning readers do of books:
```text
inSIDEoUT
```

(though not in the typical flag format, this works!)