+++
title = "be01"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/be01.zip and find a way to get the flag from the program.

It's the first easy challenge, so I'm just gonna strings it.
```bash
strings program
```
```text
...
UWVS
[^_]
Flag: sTriNGS-r-EZ-7819
I don't do anything really...
I swear I'm not hiding anything. Nothing at all.
...
```

Easy indeed, my binary friend.