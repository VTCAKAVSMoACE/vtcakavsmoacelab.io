+++
title = "SMC3"
date = 2020-06-21
+++

SMC3 was put on by the University of North Georgia and sponsored by SANS as a means to test the
capabilities of mixed-discipline ROTC students.

I am not a ROTC student -- but I'll never turn down the opportunity to compete with some military
boys.

The flyer that was sent out to us is below if you want to get a more accurate idea.

<p><iframe src="SMC3.pdf" width="100%" height="500px"></iframe></p>

Below are a list of write-ups for your viewing pleasure:
 - Web
    - [we01](we01)
    - [we04](we04)
    - [wm01](wm01)
    - [wm02](wm02)
    - [wm03](wm03)
    - [wh01](wh01)
    - [wh02](wh02)
    - [wh03](wh03)
    - [wx01](wx01)
 - Forensics
    - [fe01](fe01)
    - [fe02](fe02)
    - [fe03](fe03)
    - [fe04](fe04)
    - [fm01](fm01)
 - Networking
    - [ne01](ne01)
    - [nm01](nm01)
 - Binary Exploitation
    - [be01](be01)
    - [be02](be02)
    - [bm01](bm01)
    - [bh01](bh01)
    - [bh02](bh02)
    - [bh03](bh03)
    - [bx01](bx01)
 - Crypto
    - Ommitted as they all were just "slap it in a crypto solver". I use [quipquip](https://www.quipqiup.com), so enjoy.
    Admittedly, it did allow me to flex OSINT a little on one of them for a reverse image solver, but oh well.
