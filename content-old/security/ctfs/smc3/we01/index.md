+++
title = "we01"
+++

Description:

<blockquote>
Access the site at https://ggcs-we01.allyourbases.co and see if you can find the flag in a common directory.

Hint: Think about places you could get a list of common directories to check for.
</blockquote>

As the hint suggests, we dirbuster that sucker:
```bash
dirb https://ggcs-we01.allyourbases.co/ /usr/share/dirb/wordlists/small.txt -w
```

Surprise of all surprises, we find it! We are presented with a page which gives us a link to `flag.txt`:
```text
bustING_direTORies_8918
```
