+++
title = "nm01"
+++

This one wasn't really even a network challenge, more of just a "connect and do stuff" challenge.

Asked you to solve problems in a given time limit. My solver is below:

```python
from pwn import *

p = remote("ggcs-nm01.allyourbases.co", 6167)

try:
  while True:
    s = p.recvuntil("=")
    print(s)

    prob = s.split()
    p.sendline(str(int(prob[0]) * int(prob[2])))
    print(int(prob[0]) * int(prob[2]))
except:
  print(p.recvall())
```
```text
[+] Opening connection to ggcs-nm01.allyourbases.co on port 6167: Done
b'1337 * 42 ='
56154
b' \n42 * 42 ='
1764
[+] Receiving all data: Done (23B)
[*] Closed connection to ggcs-nm01.allyourbases.co port 6167
b' \nFlag: QUiCKDraw_2323\n'
```

It's that easy. Actually a bad challenge in my opinion -- my network speed was almost too slow to succeed at this. Folks
with worse internet might not have been able to solve it at all, which is unfortunate.