+++
title = "be02"
+++

Description:

>Download the file at https://ggcs-files.allyourbases.co/be02.zip and find out what the program does to get the flag.

We're given a C source file with some nasty operations. Can't be bothered to reverse it, just gonna compile it and run
it.

```bash
gcc program.c; ./a.out
```
```text
Flag: c0mpILE-tIME_1822
```
