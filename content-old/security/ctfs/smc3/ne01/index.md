+++
title = "ne01"
+++

This challenge was a quick and dirty nmap.
```bash
nmap -Pn -p 1-65535 -vvv ggcs-ne01.allyourbases.co
```

We find a couple with this. One with the solution is solvable with:
```bash
nc ggcs-ne01.allyourbases.co 6166
```
```text
ID: ne01
Flag: hunTingPoRTS_7727
```
