+++
title = "Create Threads w/o Send Message"
+++

The Send Messages permission is separate from Create Public/Private Threads, with the
default permission set to "Allow". As a result, an extremely common misconfiguration in
public servers with restricted channels is that users are not properly restricted from
creating threads.

![separation of permissions in configuration for channel](permissions.png)

This allows for arbitrary users to create these threads without necessarily having the
proper permissions, such as allowing spam bots to send spam into your restricted
channels.

![example misconfiguration](misconfiguration.png)

Remediation: disable Create Public/Private Threads in all channels you don't want people
to send messages in.

![example remediation](remediation.png)
