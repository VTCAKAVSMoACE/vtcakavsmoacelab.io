+++
title = "Discord (Vulnerable Misconfigurations)"
+++

This folder documents common misconfiguration issues with Discord server permissions. I'm making
it because I find that a lot of Discord servers have reeeeally bad default permissions and it's
a nice way to document how things could go wrong.

### Message Sending Permissions

 - [Create Threads without Send Message](create-threads)
